(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["users/management/display"],{

/***/ "./assets/css/views/users/management/display.css":
/*!*******************************************************!*\
  !*** ./assets/css/views/users/management/display.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/views/users/management/display.js":
/*!*****************************************************!*\
  !*** ./assets/js/views/users/management/display.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../../../../css/views/users/management/display.css */ "./assets/css/views/users/management/display.css");

$(document).ready(function () {
  $('#remove-button').on('click', function (e) {
    e.preventDefault();
    var button = $(this);
    $.bsAlert.confirmTitle = 'Usunięcie konta';
    $.bsAlert.closeDisplay = 'Nie';
    $.bsAlert.sureDisplay = 'Tak';
    $.bsAlert.confirm("Czy na pewno chcesz usunąć konto?", function () {
      $(location).attr('href', button.attr('href'));
    });
  });
});

/***/ })

},[["./assets/js/views/users/management/display.js","runtime"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL3ZpZXdzL3VzZXJzL21hbmFnZW1lbnQvZGlzcGxheS5jc3M/YmNlMyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvdmlld3MvdXNlcnMvbWFuYWdlbWVudC9kaXNwbGF5LmpzIl0sIm5hbWVzIjpbInJlcXVpcmUiLCIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsIm9uIiwiZSIsInByZXZlbnREZWZhdWx0IiwiYnV0dG9uIiwiYnNBbGVydCIsImNvbmZpcm1UaXRsZSIsImNsb3NlRGlzcGxheSIsInN1cmVEaXNwbGF5IiwiY29uZmlybSIsImxvY2F0aW9uIiwiYXR0ciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsdUM7Ozs7Ozs7Ozs7O0FDQUFBLG1CQUFPLENBQUMsMkdBQUQsQ0FBUDs7QUFDQUMsQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFVO0FBQzNCRixHQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQkcsRUFBcEIsQ0FBdUIsT0FBdkIsRUFBK0IsVUFBU0MsQ0FBVCxFQUFXO0FBQ3pDQSxLQUFDLENBQUNDLGNBQUY7QUFDQSxRQUFJQyxNQUFNLEdBQUdOLENBQUMsQ0FBQyxJQUFELENBQWQ7QUFDQUEsS0FBQyxDQUFDTyxPQUFGLENBQVVDLFlBQVYsR0FBeUIsaUJBQXpCO0FBQ0FSLEtBQUMsQ0FBQ08sT0FBRixDQUFVRSxZQUFWLEdBQXlCLEtBQXpCO0FBQ0FULEtBQUMsQ0FBQ08sT0FBRixDQUFVRyxXQUFWLEdBQXdCLEtBQXhCO0FBQ0FWLEtBQUMsQ0FBQ08sT0FBRixDQUFVSSxPQUFWLENBQWtCLG1DQUFsQixFQUFzRCxZQUFVO0FBQy9EWCxPQUFDLENBQUNZLFFBQUQsQ0FBRCxDQUFZQyxJQUFaLENBQWlCLE1BQWpCLEVBQXlCUCxNQUFNLENBQUNPLElBQVAsQ0FBWSxNQUFaLENBQXpCO0FBQ0EsS0FGRDtBQUdBLEdBVEQ7QUFVQSxDQVhELEUiLCJmaWxlIjoidXNlcnMvbWFuYWdlbWVudC9kaXNwbGF5LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIiwicmVxdWlyZSgnLi4vLi4vLi4vLi4vY3NzL3ZpZXdzL3VzZXJzL21hbmFnZW1lbnQvZGlzcGxheS5jc3MnKTtcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XG5cdCQoJyNyZW1vdmUtYnV0dG9uJykub24oJ2NsaWNrJyxmdW5jdGlvbihlKXtcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0dmFyIGJ1dHRvbiA9ICQodGhpcyk7XG5cdFx0JC5ic0FsZXJ0LmNvbmZpcm1UaXRsZSA9ICdVc3VuacSZY2llIGtvbnRhJztcblx0XHQkLmJzQWxlcnQuY2xvc2VEaXNwbGF5ID0gJ05pZSc7XG5cdFx0JC5ic0FsZXJ0LnN1cmVEaXNwbGF5ID0gJ1Rhayc7XG5cdFx0JC5ic0FsZXJ0LmNvbmZpcm0oXCJDenkgbmEgcGV3bm8gY2hjZXN6IHVzdW7EhcSHIGtvbnRvP1wiLGZ1bmN0aW9uKCl7XG5cdFx0XHQkKGxvY2F0aW9uKS5hdHRyKCdocmVmJywgYnV0dG9uLmF0dHIoJ2hyZWYnKSlcblx0XHR9KTtcblx0fSk7XG59KTtcbiJdLCJzb3VyY2VSb290IjoiIn0=